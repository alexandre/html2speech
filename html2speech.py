#! /usr/bin/env python2


# html2speech.py: a script to read outloud an HTML file.

# Copyright (C) 2015 Alexandre Leray

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import html5lib
from html5lib.filters.base import Filter
import pyttsx


class MyFilter(Filter):
    def __init__(self, source, engine):
        self.source = source
        self.engine = engine
        self.engine.setProperty('voice', 'mb-nl2')
        self.rate = 130
        self.volume = 1
        self.indentation = 0
        self.engine.setProperty('rate', self.rate)
        self.engine.setProperty('volume', self.volume)

    def __iter__(self):
        for token in Filter.__iter__(self):
            type = token["type"]
            rate = self.engine.getProperty('rate')
            print(rate)

            if type == "StartTag" and token["name"] == "h1":
                self.volume += 1
                self.engine.setProperty('volume', self.volume)
                self.rate -= 50
                self.engine.setProperty('rate', self.rate)

            elif type == "EndTag" and token["name"] == "h1":
                self.volume -= 1
                self.engine.setProperty('volume', self.volume)
                self.rate += 50
                self.engine.setProperty('rate', self.rate)

            if type == "StartTag" and token["name"] == "h2":
                self.volume += 0.5
                self.engine.setProperty('volume', self.volume)

            elif type == "EndTag" and token["name"] == "h2":
                self.volume -= 0.5
                self.engine.setProperty('volume', self.volume)

            if type == "StartTag" and token["name"] == "em":
                self.engine.setProperty('voice', 'mb-fr4')

            elif type == "EndTag" and token["name"] == "em":
                self.engine.setProperty('voice', 'mb-fr1')

            if type == "StartTag" and token["name"] == "li":
                self.indentation += 1
                self.engine.say("*" * self.indentation)

            elif type == "EndTag" and token["name"] == "li":
                self.indentation -= 1

            elif type == "Characters":
                self.engine.say(token["data"])

            yield token


def main(html):
    engine = pyttsx.init()
    tree = html5lib.parse(html, treebuilder="etree", namespaceHTMLElements=False)
    walker = html5lib.getTreeWalker("etree")
    stream = walker(tree)
    stream = MyFilter(stream, engine)
    s = html5lib.serializer.HTMLSerializer(quote_attr_values="always",
                                            omit_optional_tags=False)

    output = s.render(stream)

    engine.runAndWait()


if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(description='Reads an HTML file outloud')

    parser.add_argument('infile',  nargs='?', type=argparse.FileType('r'), default=sys.stdin,
                        help="the source `.html` file")
    args = parser.parse_args()


    content = args.infile.read()
    try:
        unicode_content = content.decode("utf-8")
    except UnicodeDecodeError:
        unicode_content = content.decode("iso8559-1")

    main(unicode_content)
